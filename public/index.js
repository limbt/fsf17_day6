//for client side we use IIFE to execute the application

(function() {
//Everything inside this function is our application
//Create an instance of Angular applcation/module
// 1st param: app's name, this is the apps dependencies

var FirstApp = angular.module("FirstApp", []);

var reverse = function(s){
    return s.split("").reverse().join("");
}
var taskarray =[];

// populate with sample data 
taskarray = [   {"Task":"Buy fruits","Date":"02/18/17", "Cat":"urgent","Done":"false" },
                {"Task":"Maintain Car","Date":"04/18/17", "Cat":"normal","Done":"false"  },
                {"Task":"Buy Movie Ticket","Date":"03/20/17", "Cat":"normal", "Done":"false" },
                {"Task":"Reply email","Date":"02/20/17", "Cat":"normal", "Done":"false" },
                {"Task":"Work on Project","Date":"02/14/17", "Cat":"urgent","Done":"false" },
                {"Task":"Pay Bill","Date":"03/05/17", "Cat":"important", "Done":"false" },
                {"Task":"Pay Bill","Date":"01/05/17", "Cat":"important", "Done":"true" },
            ];

var FirstCtrl = function() {
    //Hold a reference of this controller so that when 'this' changes we are 
    //still referncing the controller
    var firstCtrl = this;
    console.log("Hello");

    //Define first model of this controller/vm 
    firstCtrl.task="";
    firstCtrl.duedate ="";
    firstCtrl.cat ="";
    firstCtrl.done ="false";
    firstCtrl.err="";

    // define another model 
    firstCtrl.result = taskarray;

    // define an event for the click button
  
  // validate due date is correct format if not return message
    // firstCtrl.validateInputs = function() {
    //     x = firstCtrl.duedate;
    //             if ( x ) {
    //         return true;
    //     } else {
    //         firstCtrl.err = "Please key in date format MM/DD/YY";
    //         return false;
    //     }
    //}

    firstCtrl.clear = function(){
        firstCtrl.result = "";
        firstCtrl.err ="";
    };

    
    firstCtrl.add = function(){
        if (true) {
            taskarray.push( {"Task":firstCtrl.task, "Cat":firstCtrl.cat, "Date":firstCtrl.duedate, "Done": firstCtrl.done  } );
            console.log(taskarray);
        }; 
    };

   // To remove record

    firstCtrl.removeRow = function(clickindex){		
       	if( clickindex === -1 ) {
			alert( "Something gone wrong" );
		}
       
		firstCtrl.result.splice( clickindex, 1 );		
	};




    
    firstCtrl.sortTask = function(){
                taskarray.sort(function(a, b) {
                var TaskA = a.Task.toUpperCase(); // ignore upper and lowercase
                var TaskB = b.Task.toUpperCase(); // ignore upper and lowercase
                
                if (TaskA < TaskB) {
                    return -1;
                    }
                if (TaskA > TaskB) {
                    return 1;
                    }

                // names must be equal
                return 0;
            });
            firstCtrl.result = taskarray;
        };

           
    firstCtrl.sortCat = function(){
                taskarray.sort(function(a, b) {
                var TaskA = a.Cat.toUpperCase(); // ignore upper and lowercase
                var TaskB = b.Cat.toUpperCase(); // ignore upper and lowercase
                
                if (TaskA < TaskB) {
                    return -1;
                    }
                if (TaskA > TaskB) {
                    return 1;
                    }

                // names must be equal
                return 0;
            });
            firstCtrl.result = taskarray;
        };


    firstCtrl.sortDate = function(){
         taskarray.sort(function(a, b) {
                var TaskA = a.Date.toUpperCase(); // ignore upper and lowercase
                var TaskB = b.Date.toUpperCase(); // ignore upper and lowercase
                
                if (TaskA < TaskB) {
                    return -1;
                    }
                if (TaskA > TaskB) {
                    return 1;
                    }

                // names must be equal
                return 0;
            });
            firstCtrl.result = taskarray;
        };


    


    // define an event for the clear text
    firstCtrl.clearText = function(){
        firstCtrl.reverseMyText ="";    
        };
    
    }; 
   
       

    //Define a controller call firstCtrl
    FirstApp.controller("FirstCtrl", [FirstCtrl])
})();
