var express = require("express");
var path = require("path");
var app = express();

app.use(express.static(path.join (__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, 'bower_components')));

app.set("port", parseInt(process.argv[2]) || process.env.APP_PORT || 3000);

app.listen(app.get('port'), function(req, res){
    console.log("Application started at %s on port %d", new Date(), app.get('port'));
});

